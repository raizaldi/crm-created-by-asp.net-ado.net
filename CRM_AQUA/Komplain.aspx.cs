﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA
{
    public partial class Komplain : System.Web.UI.Page
    {

        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);
            nilaiTertinggi();
            idpelanggan.Text = Session["id_pelanggan"].ToString();
        }

        public void simpanKomplain()
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [crm_aqua].[dbo].[komplain]
                                           ([no_komplain]
                                           ,[keterangan]
                                           ,[id_pelanggan])
                                     VALUES
                                           ('"+no.Text+"','"+keterangan.Text+"','"+idpelanggan.Text+"')";
                cmd.ExecuteNonQuery();
                no.Text = "";
                keterangan.Text = "";
                idpelanggan.Text = "";
                conn.Close();
                nilaiTertinggi();
            }
            catch (Exception) { }
        }

        public void nilaiTertinggi()
        {
            try
            {
                int hitung;
                string urut;
                conn.Open();
                string sql = @"SELECT count(no_komplain) as no FROM [komplain]";
                cmd = new SqlCommand(sql, conn);
                dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    urut = dr["no"].ToString();
                    hitung = int.Parse(urut);
                    hitung = hitung + 1;
                    no.Text = hitung.ToString();

                }
                else
                {
                    no.Text = "1";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                no.Text = ex.Message;
            }
        }

        protected void Kirim_Click(object sender, EventArgs e)
        {
            simpanKomplain();
        }
    }
}