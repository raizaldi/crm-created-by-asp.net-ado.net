﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA.Admin
{
    public partial class pelanggan : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["nama_pelanggan"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilpesanan();
        }

        protected void Simpan_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [pelanggan]
                                   ([nama_pelanggan]
                                   ,[alamat_pelanggan]
                                   ,[ktp]
                                   ,[npwp]
                                   ,[no_telpon]
                                   ,[username]
                                   ,[password]
                                   ,[tgl_berlangganan]
                                   ,[hak_akses])
                             VALUES
                                   ('"+nama.Text+"','"+alamat.Text+"','"+ktp.Text+"','"+npwp.Text+"','"+kontak.Text+"','"+username.Text+"','"+password.Text+"','"+tglgabung.Text+"','"+hak_akses.SelectedItem.Value+"')";
                cmd.ExecuteNonQuery();
                nama.Text = "";
                alamat.Text = "";
                ktp.Text = "";
                npwp.Text = "";
                kontak.Text = "";
                username.Text = "";
                password.Text = "";
                tglgabung.Text = "";
                tampilpesanan();
                conn.Close();
            }
            catch (Exception ex)
            {
                nama.Text = ex.Message;
   
            }
        }

        public void tampilpesanan()
        {
            string sql = @"SELECT [id_pelanggan]
                        ,[nama_pelanggan]
                        ,[alamat_pelanggan]
                        ,[ktp]
                        ,[npwp]
                        ,[no_telpon]
                        ,[username]
                        ,[password]
                        ,[tgl_berlangganan]
                        ,[hak_akses]
                        ,[nama_sales]
                    FROM  [crm_aqua].[dbo].[pelanggan] where [tgl_berlangganan] between '" + dari.Text+"' and '"+sampai.Text+"'";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            Listpelanggan.DataSource = dt;
            Listpelanggan.DataBind();
            cmd.Dispose();
            conn.Close();
        }


        protected void Inkpilih_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["id_pelanggan"] = btn.CommandArgument;
            conn.Open();
            string sql = @"SELECT [id_pelanggan]
                      ,[nama_pelanggan]
                      ,[alamat_pelanggan]
                      ,[ktp]
                      ,[npwp]
                      ,[no_telpon]
                      ,[username]
                      ,[password]
                      ,[tgl_berlangganan]
                      ,[hak_akses] FROM [pelanggan] where [id_pelanggan]='" + Session["id_pelanggan"] + "'";
            cmd = new SqlCommand(sql, conn);
            //da = new SqlDataAdapter(cmd);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    nama.Text = dr["nama_pelanggan"].ToString();
                    alamat.Text = dr["alamat_pelanggan"].ToString();
                    ktp.Text = dr["ktp"].ToString();
                    npwp.Text = dr["npwp"].ToString();
                    kontak.Text = dr["no_telpon"].ToString();
                    username.Text = dr["username"].ToString();
                    password.Text = dr["password"].ToString();
                    tglgabung.Text = dr["tgl_berlangganan"].ToString();
                    
                }
            }
        }

        protected void ubah_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE [pelanggan] SET [nama_pelanggan] = '" + nama.Text + "' ,[alamat_pelanggan] = '" + alamat.Text + "',[ktp] = '" + ktp.Text + "' ,[npwp] = '" + npwp.Text + "',[no_telpon] = '" + kontak.Text + "',[username] = '" + username.Text + "',[password] = '" + password.Text + "',[tgl_berlangganan] = '" + tglgabung.Text + "',[hak_akses] = '" + hak_akses .SelectedItem.Value+ "' where [id_pelanggan] = '" + Session["id_pelanggan"] + "' ";
                cmd.ExecuteNonQuery();
                nama.Text = "";
                alamat.Text = "";
                ktp.Text = "";
                npwp.Text = "";
                kontak.Text = "";
                username.Text = "";
                password.Text = "";
                tglgabung.Text = "";
                tampilpesanan();
                conn.Close();
            }
            catch (Exception ex)
            {
                nama.Text = ex.Message;

            }
        }

        protected void hapus_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"DELETE FROM [pelanggan] where [id_pelanggan] = '" + Session["id_pelanggan"] + "' ";
                cmd.ExecuteNonQuery();
                nama.Text = "";
                alamat.Text = "";
                ktp.Text = "";
                npwp.Text = "";
                kontak.Text = "";
                username.Text = "";
                password.Text = "";
                tglgabung.Text = "";
                tampilpesanan();
                conn.Close();
            }
            catch (Exception ex)
            {
                nama.Text = ex.Message;

            }
        }

        protected void btncari_Click(object sender, EventArgs e)
        {
            tampilpesanan();
        }
    }

}