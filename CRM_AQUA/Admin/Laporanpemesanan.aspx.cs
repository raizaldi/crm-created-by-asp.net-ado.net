﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA.Admin
{
    public partial class Laporanpemesanan : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["nama_pelanggan"] == null)
            {
                Response.Redirect("Login.aspx");
            }
           
           /* tampilpesanan();
            total();
            baranglaku();*/
        }


    

        public void tampilpesanan()
        {
            string sql = @"SELECT [no_pemesanan] as [no]
                          ,[tgl_pemesanan] as [tgl]
                          ,[id_pelanggan] as [id]
                          ,[total_pemesanan] as [jumlah] FROM [H_pemesanan] where [tgl_pemesanan] between '" + dari.Text + "' and '" + sampai.Text + "'";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            ListLaporan.DataSource = dt;
            ListLaporan.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        public void total()
        {
            string sql = @"select SUM(total_pemesanan) as total  from [H_pemesanan] where [tgl_pemesanan] between '"+dari.Text+"' and '"+sampai.Text+"'";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            sublaporan.DataSource = dt;
            sublaporan.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        public void baranglaku()
        {
            string sql = @"SELECT distinct a.id_produk as kode, (select COUNT(quantity) from D_pemesanan where id_produk=a.id_produk ) as jumlah
 FROM [D_pemesanan] a inner join [H_pemesanan] b on a.no_pemesanan=b.no_pemesanan where b.tgl_pemesanan between '"+dari.Text+"' and '"+sampai.Text+"'";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            listlaku.DataSource = dt;
            listlaku.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        protected void simpan_Click(object sender, EventArgs e)
        {
            tampilpesanan();
            total();
            baranglaku();
        }
    }
}