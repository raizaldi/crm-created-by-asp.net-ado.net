﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Produk.aspx.cs" Inherits="CRM_AQUA.Admin.Produk" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="form-group">
     <asp:TextBox ID="id" runat="server" class="form-control" placeholder="Id Produk"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="nama" runat="server" class="form-control" placeholder="Nama Produk"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:DropDownList ID="jenis_produk" runat="server" class="form-control">
        <asp:ListItem Value="">.:-Jenis Produk-:.</asp:ListItem>
        <asp:ListItem Value="Air Mineral">Air Mineral</asp:ListItem>
        <asp:ListItem Value="Isotonik">Isotonik</asp:ListItem>
      </asp:DropDownList>
  </div>
  <div class="form-group">
     <asp:TextBox ID="harga" runat="server" class="form-control" placeholder="Harga Produk"></asp:TextBox>
  </div>
  <div class="form-group">
    <asp:FileUpload class="form-control" ID="uploadgambar" runat="server"/>
  </div>
  <div class="form-group">
     <asp:DropDownList ID="member_produk" runat="server" class="form-control">
        <asp:ListItem Value="">.:-Jenis Member-:.</asp:ListItem>
        <asp:ListItem Value="Ya">ya</asp:ListItem>
        <asp:ListItem Value="Tidak">Tidak</asp:ListItem>
      </asp:DropDownList>
  </div>
<asp:Button ID="Simpan" runat="server" class="btn btn-success" Text="Simpan" 
      onclick="Simpan_Click" />

&nbsp;
    <asp:Button ID="Ubah" runat="server" Text="Ubah" class="btn btn-warning" 
        onclick="Ubah_Click" />
    <asp:Button ID="Hapus" runat="server" Text="Hapus" class="btn btn-danger" 
        onclick="Hapus_Click" />
<br /><br /><br />

 <asp:ListView ID="Listproduk" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="danger"><center> Produk</center></th>
                    <th class="danger"><center>Nama Produk</center></th>
                    <th class="danger"><center>Jenis Produk</center></th>
                    <th class="danger"><center>Harga</center></th>
                    <th class="danger"><center>Foto</center></th>
                    <th class="danger"><center>Member</center></th>
                    <th class="danger">Aksi</th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center"><%# Eval("id_produk")%></td>
                    <td align="center"><%# Eval("nama_produk")%></td>
                    <td align="center"><%# Eval("jenis_produk")%></td>
                    <td align="center"><%# Eval("harga")%></td>
                    <td align="center"><img src="../images/<%# Eval("foto")%>" width="50px" height="30px" class="img-rounded"></td>
                    <td align="center"><%# Eval("member")%></td>
                    <td>
                        <asp:LinkButton class="btn btn-info" ID="Inkselect" runat="server" CommandArgument='<%# Eval("id_produk")%>' OnClick="Inkpilih_Click" >Pilih</asp:LinkButton>
                    </td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>

</asp:Content>
