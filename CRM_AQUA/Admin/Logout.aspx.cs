﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM_AQUA.Admin
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["nama_pelanggan"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            Session.Clear();
            Response.Redirect("Login.aspx");
        }
    }
}