﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Komplain.aspx.cs" Inherits="CRM_AQUA.Admin.Komplain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<form>
  <div class="form-group">
    <label for="exampleInputEmail1">No Komplain</label>
      <asp:TextBox ID="no" runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Keterangan</label>
      <asp:TextBox ID="keterangan" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Id Pelanggan</label>
      <asp:TextBox ID="idpelanggan"  runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Konfirmasi</label>
      <asp:TextBox ID="konfirmasi" TextMode="MultiLine" ToolTip="Konfirmasi Atas Komplain"  runat="server" class="form-control"></asp:TextBox>
  </div>
 <asp:Button ID="Kirim" runat="server" class="btn btn-info" Text="Simpan" 
      onclick="Kirim_Click" />
</form>
<br /><br /><br />
 <asp:ListView ID="Listkomplain" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="danger"><center> No Komplain</center></th>
                    <th class="danger"><center>Keterangan</center></th>
                    <th class="danger"><center>Id Pelanggan</center></th>
                    <th class="danger"><center>Konfirmasi</center></th>
                    <th class="danger"><center>Aksi</center></th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center"><%# Eval("no_komplain")%></td>
                    <td align="center"><%# Eval("keterangan")%></td>
                    <td align="center"><%# Eval("id_pelanggan")%></td>
                    <td align="center"><%# Eval("konfirmasi")%></td>
                    <td align="center">
                        <asp:LinkButton class="btn btn-info" ID="Inkselect" runat="server" CommandArgument='<%# Eval("no_komplain")%>' OnClick="Inkpilih_Click" >Pilih</asp:LinkButton>
                   </td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>

</asp:Content>
