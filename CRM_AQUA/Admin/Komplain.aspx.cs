﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA.Admin
{
    public partial class Komplain : System.Web.UI.Page
    {

        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["nama_pelanggan"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilpesanan();
        }

        public void tampilpesanan()
        {
            string sql = @"SELECT [no_komplain]
                          ,[keterangan]
                          ,[id_pelanggan]
                          ,[konfirmasi]
                      FROM [crm_aqua].[dbo].[komplain]";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            Listkomplain.DataSource = dt;
            Listkomplain.DataBind();
            cmd.Dispose();
            conn.Close();
        }


        protected void Inkpilih_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["no_komplain"] = btn.CommandArgument;
            conn.Open();
            string sql = @"SELECT [no_komplain]
                          ,[keterangan]
                          ,[id_pelanggan]
                          ,[konfirmasi]
                      FROM [komplain] WHERE [no_komplain] ='" + Session["no_komplain"] + "'";
                        cmd = new SqlCommand(sql, conn);
            //da = new SqlDataAdapter(cmd);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    no.Text          = dr["no_komplain"].ToString();
                    keterangan.Text  = dr["keterangan"].ToString();
                    idpelanggan.Text = dr["id_pelanggan"].ToString();
                    konfirmasi.Text  = dr["konfirmasi"].ToString();
                }
            }
        }

        protected void Kirim_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE [crm_aqua].[dbo].[komplain] SET [keterangan] = '"+keterangan.Text+"',[id_pelanggan] = '"+idpelanggan.Text+"' ,[konfirmasi] = '"+konfirmasi.Text+"' WHERE [no_komplain] = '"+Session["no_komplain"]+"'";
                cmd.ExecuteNonQuery();
                no.Text = "";
                keterangan.Text = "";
                idpelanggan.Text = "";
                konfirmasi.Text = "";
                tampilpesanan();
                conn.Close();
            }
            catch (Exception ex)
            {

                no.Text = ex.Message;
            }
        }
    }
}