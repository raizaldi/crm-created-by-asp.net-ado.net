﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="pelanggan.aspx.cs" Inherits="CRM_AQUA.Admin.pelanggan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form>
  <div class="form-group">
     <asp:TextBox ID="nama" runat="server" class="form-control" placeholder="Nama pelanggan" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="alamat" TextMode="MultiLine" runat="server" class="form-control" placeholder="Alamat pelanggan" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="ktp" runat="server" class="form-control" placeholder="Ktp pelanggan" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="npwp" runat="server" class="form-control" placeholder="Npwp pelanggan" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="kontak" runat="server" class="form-control" placeholder="Kontak pelanggan" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="username" runat="server" class="form-control" placeholder="Username" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="password" TextMode="Password" runat="server" class="form-control" placeholder="Password" Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
     <asp:TextBox ID="tglgabung" TextMode="date" runat="server" class="form-control"  Width="400px"></asp:TextBox>
  </div>
  <div class="form-group">
      <asp:DropDownList ID="hak_akses" runat="server" class="form-control" Width="400px">
        <asp:ListItem Value="">.:-Hak Akses-:.</asp:ListItem>
        <asp:ListItem Value="Admin">Administrator</asp:ListItem>
        <asp:ListItem Value="Pelanggan">Pelanggan</asp:ListItem>
      </asp:DropDownList>
  </div>
<asp:Button ID="Simpan" runat="server" class="btn btn-success" Text="Simpan" 
      onclick="Simpan_Click" />
<asp:Button ID="ubah" runat="server" class="btn btn-warning" Text="Ubah" 
        onclick="ubah_Click" />
<asp:Button ID="hapus" runat="server" class="btn btn-danger" Text="Hapus" 
        onclick="hapus_Click" />
</form>
&nbsp;
<br /><br /><br />
Filtering Data Pelanggan
<asp:TextBox ID="dari" runat="server" TextMode="Date"></asp:TextBox> Sampai
<asp:TextBox ID="sampai" runat="server" TextMode="Date"></asp:TextBox>
<asp:Button ID="btncari" runat="server" class="btn btn-danger" text="cari" 
        onclick="btncari_Click"/>
<br /><br /><br />
<div id="print-area">
<center><h1>Laporan Penjualan</h1></center>
 <asp:ListView ID="Listpelanggan" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="info">Id Pelanggan</th>
                    <th class="info">Nama Pelanggan</th>
                    <th class="info">Alamat</th>
                    <th class="info">Ktp</th>
                    <th class="info">Npwp</th>
                    <th class="info">No Telpon</th>
                    <th class="info">Username</th>
                    <th class="info">Password</th>
                    <th class="info">Tanggal Berlangganan</th>
                    <th class="info">Level</th>
                     <th class="info">Nama Sales</th>
                    <th  class="info">Aksi</th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td><%# Eval("id_pelanggan")%></td>
                    <td><%# Eval("nama_pelanggan")%></td>
                    <td><%# Eval("alamat_pelanggan")%></td>
                    <td><%# Eval("ktp")%></td>
                    <td><%# Eval("npwp")%></td>
                    <td><%# Eval("no_telpon")%></td>
                    <td><%# Eval("username")%></td>
                    <td><%# Eval("password")%></td>
                    <td><%# Eval("tgl_berlangganan")%></td>
                    <td><%# Eval("hak_akses")%></td>
                     <td><%# Eval("nama_sales")%></td>
                    <td>
                        <asp:LinkButton class="btn btn-info" ID="Inkselect" runat="server" CommandArgument='<%# Eval("id_pelanggan")%>' OnClick="Inkpilih_Click" >Pilih</asp:LinkButton>
                    </td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>
</div>
<button type="button" onclick="printDiv('print-area')" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print </button>
</asp:Content>
