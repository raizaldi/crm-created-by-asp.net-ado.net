﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Laporanpemesanan.aspx.cs" Inherits="CRM_AQUA.Admin.Laporanpemesanan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:TextBox ID="dari" runat="server" TextMode="Date"></asp:TextBox>
Sampai
<asp:TextBox ID="sampai" runat="server" TextMode="Date"></asp:TextBox>
<asp:Button ID="simpan" runat="server" class="btn btn-success" Text="Cari" 
        Height="33px" onclick="simpan_Click" Width="90px" />

<div id="print-area">
<h3>Total Pemesanan</h3>
<asp:ListView ID="ListLaporan" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="danger"><center> No</center></th>
                    <th class="danger"><center>Tanggal Pesan</center></th>
                    <th class="danger"><center>Id Pelanggan</center></th>
                    <th class="danger"><center>Jumlah Pembelian</center></th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center"><%# Eval("no")%></td>
                    <td align="center"><%# Eval("tgl")%></td>
                    <td align="center"><%# Eval("id")%></td>
                    <td align="center"><%# Eval("jumlah")%></td>
                </tr>
                
               
            </ItemTemplate>
        </asp:ListView>


        
<asp:ListView ID="sublaporan" runat="server">
            <LayoutTemplate>
                <table class="table table-striped">
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
             </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td colspan="18">Total Pemesanan</td>
                    <td></td>
                     <td></td>
                      <td></td>
                    <td align="center"><b>Rp.<%# Eval("total")%></b></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

<h3>Kode barang terlaku</h3>

<asp:ListView ID="listlaku" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="info" width="90px"><center> Kode Produk</center></th>
                    <th class="info" width="90px"><center>Jumlah Pesan</center></th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td align="center" width="90px"><%# Eval("kode")%></td>
                    <td align="center" width="90px"><%# Eval("jumlah")%></td>
                    
                </tr>
                
               
            </ItemTemplate>
        </asp:ListView>
</div>

<button type="button" onclick="printDiv('print-area')" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print  </button>
</asp:Content>
