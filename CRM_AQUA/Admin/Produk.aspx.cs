﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA.Admin
{
    public partial class Produk : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["nama_pelanggan"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            tampilpesanan();
        }

        public bool CekTipeFile(string fileName)
        {
            string ekstensi = Path.GetExtension(fileName).ToLower();
            switch (ekstensi)
            {
                case ".gif":
                case ".jpg":
                case ".jpeg":
                case ".png":
                    return true;
                default:
                    return false;
            }
        }

        protected void Simpan_Click(object sender, EventArgs e)
        {
            if (uploadgambar.HasFile)
            {
                if (CekTipeFile(uploadgambar.FileName))
                {
                    string strUpload = Path.Combine("~/images/", uploadgambar.FileName);
                    strUpload = MapPath(strUpload);
                    uploadgambar.SaveAs(strUpload);
                }
            }

            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO [produk]
                               ([id_produk]
                               ,[nama_produk]
                               ,[jenis_produk]
                               ,[harga]
                               ,[foto]
                               ,[member])
                         VALUES
                               ('"+id.Text+"','"+nama.Text+"','"+jenis_produk.SelectedItem.Value+"','"+harga.Text+"','"+uploadgambar.FileName+"','"+member_produk.SelectedItem.Value+"')";
            cmd.ExecuteNonQuery();
            id.Text = "";
            nama.Text = "";
            jenis_produk.SelectedItem.Value = "";
            member_produk.SelectedItem.Value = "";
            harga.Text = "";
            conn.Close();
            tampilpesanan();
        }

        public void tampilpesanan()
        {
            string sql = @"SELECT [id_produk]
                          ,[nama_produk]
                          ,[jenis_produk]
                          ,[harga]
                          ,[foto]
                          ,[member]
                      FROM [produk]";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            Listproduk.DataSource = dt;
            Listproduk.DataBind();
            cmd.Dispose();
            conn.Close();
        }

        protected void Inkpilih_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["id_produk"] = btn.CommandArgument;
            conn.Open();
            string sql = @"SELECT [id_produk]
                              ,[nama_produk]
                              ,[jenis_produk]
                              ,[harga]
                              ,[foto]
                              ,[member]
                          FROM [produk] where [id_produk]='" + Session["id_produk"] + "'";
            cmd = new SqlCommand(sql, conn);
            //da = new SqlDataAdapter(cmd);
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    id.Text = dr["id_produk"].ToString();
                    nama.Text = dr["nama_produk"].ToString();
                    harga.Text = dr["harga"].ToString();
                    
                }
            }
        }

        protected void Ubah_Click(object sender, EventArgs e)
        {
            if (uploadgambar.HasFile)
            {
                if (CekTipeFile(uploadgambar.FileName))
                {
                    string strUpload = Path.Combine("~/images/", uploadgambar.FileName);
                    strUpload = MapPath(strUpload);
                    uploadgambar.SaveAs(strUpload);
                }
            }

            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"UPDATE [produk]SET [nama_produk] = '" + nama.Text + "',[jenis_produk] = '" + jenis_produk.SelectedItem.Value + "',[harga] = '" + harga.Text + "',[foto] = '" + uploadgambar.FileName + "',[member] = '" + member_produk.SelectedItem.Value + "' WHERE [id_produk] = '" + Session["id_produk"] + "'";
            cmd.ExecuteNonQuery();
            id.Text = "";
            nama.Text = "";
            jenis_produk.SelectedItem.Value = "";
            member_produk.SelectedItem.Value = "";
            harga.Text = "";
            tampilpesanan();
            conn.Close();
            
        }

        protected void Hapus_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"DELETE FROM [produk] WHERE [id_produk] = '" + Session["id_produk"] + "'";
            cmd.ExecuteNonQuery();
            id.Text = "";
            nama.Text = "";
            jenis_produk.SelectedItem.Value = "";
            member_produk.SelectedItem.Value = "";
            harga.Text = "";
            tampilpesanan();
            conn.Close();
        }
    }
}