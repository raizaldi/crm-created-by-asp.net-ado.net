﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/userPage.Master" AutoEventWireup="true" CodeBehind="Pemesanan.aspx.cs" Inherits="CRM_AQUA.user.Pemesanan" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 160px;
        }
        .style10
        {
            width: 169px;
            height: 23px;
        }
        .style11
        {
            width: 32px;
            height: 23px;
        }
        .style12
        {
            height: 23px;
            width: 247px;
        }
        .style13
        {
            width: 169px;
            height: 42px;
        }
        .style14
        {
            width: 32px;
            height: 42px;
        }
        .style15
        {
            height: 42px;
            width: 247px;
        }
        .style16
        {
            width: 169px;
            height: 38px;
        }
        .style17
        {
            width: 32px;
            height: 38px;
        }
        .style18
        {
            height: 35px;
            width: 247px;
        }
        .style19
        {
            width: 169px;
            height: 35px;
        }
        .style20
        {
            width: 32px;
            height: 35px;
        }
        .style21
        {
            height: 38px;
            width: 247px;
        }
        .style22
        {
            height: 38px;
        }
        .style23
        {
            height: 9px;
            width: 113px;
        }
        .style24
        {
            width: 113px;
        }
        .style26
        {
            height: 9px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <table class="style1">
    <tr>
        <div class="form-group">
            <td class="style16">
                <asp:Label ID="Label6" runat="server" Text="No Pemesanan"></asp:Label>
            </td>
            <td class="style17">
                </td>
            <td class="style21">
                <asp:TextBox ID="nomor" runat="server" Width="238px" class="form-control"></asp:TextBox>
            </td>
            
        </div>
        </tr>
        <tr>
        <div class="form-group">
            <td class="style16">
                <asp:Label ID="Label1" runat="server" Text="No Produk"></asp:Label>
            </td>
            <td class="style17">
                </td>
            <td class="style21">
                <asp:TextBox ID="nopro" runat="server" Width="238px" class="form-control"></asp:TextBox>
            </td>
            <td class="style22"><asp:Button ID="Button2" runat="server" Text="Cari" 
                    class="btn btn-info" onclick="Button2_Click"  /></td>
        </div>
        </tr>
        <tr>
        <div class="form-group">
            <td class="style19">
                <asp:Label ID="Label2" runat="server" Text="Nama Produk"></asp:Label>
            </td>
            <td class="style20">
                </td>
            <td class="style18">
                <asp:TextBox ID="napro" runat="server" Width="238px" class="form-control"></asp:TextBox>
            </td>
        </div>
        </tr>
        <tr>
        <div class="form-group">
            <td class="style13">
                <asp:Label ID="Label3" runat="server" Text="Harga Produk"></asp:Label>
            </td>
            <td class="style14">Rp.
                </td>
            <td class="style15">
                <asp:TextBox ID="hapro" runat="server" Width="238px" class="form-control"></asp:TextBox>
            </td>
        </div>
        </tr>
        <tr>
        <div class="form-group">
            <td class="style10">
                <asp:Label ID="Label4" runat="server" Text="Jumlah Pesanan"></asp:Label>
            </td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
                <asp:TextBox ID="jumpe" runat="server" Width="238px" class="form-control" placeholder="Klik Enter untuk menjumlahkan"></asp:TextBox>
            </td>
            
        </div>
        </tr>
        <tr>
        <div class="form-group">
            <td class="style13">
                <asp:Label ID="Label5" runat="server" Text="Jumlah Harga"></asp:Label>
            </td>
            <td class="style14"> Rp. 
                </td>
            <td class="style15">
              <asp:TextBox ID="juha" runat="server" Width="238px" class="form-control"></asp:TextBox>
            </td>
        </div>
        </tr>
        <tr>

            <td class="style10">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
                <asp:Button ID="Button1" runat="server" Text="Pesan" Width="94px" 
                    class="btn btn-primary" onclick="Button1_Click"/>
            </td>
        </tr>
        <tr>
            <td class="style10">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
                &nbsp;</td>
        </tr>
    </table>

     <asp:ListView ID="detailpesanan" runat="server">
            <LayoutTemplate>
                <table class="table table-bordered">
                    <thead>
                    <th class="info">No</th>
                    <th class="info">Id Produk</th>
                    <th class="info">Nama Produk</th>
                    <th class="info">Quantity</th>
                    <th class="info">Harga Produk</th>
                    <th class="info">Jumlah Harga</th>
                    </thead>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
                </LayoutTemplate>
             
            <ItemTemplate>
             
                <tr>
                    <td><%# Eval("no")%></td>
                    <td><%# Eval("id")%></td>
                    <td><%# Eval("np")%></td>
                    <td><%# Eval("qty")%></td>
                    <td><%# Eval("hg")%></td>
                    <td><%# Eval("jml")%></td>
                </tr>
               
            </ItemTemplate>
        </asp:ListView>


  <div class="form-group">
    <label for="exampleInputEmail1">Id Pelanggan</label>
      <asp:TextBox ID="idpelanggan" runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Total Pembayaran</label>
      <asp:TextBox ID="total" runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Tanggal Pemesanan</label>
      <asp:TextBox ID="tgl" TextMode="Date" runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Tanggal Pengiriman</label>
      <asp:TextBox ID="tanggalkirim" TextMode="Date" runat="server" class="form-control"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Alamat Pengiriman</label>
      <asp:TextBox ID="alamat" ToolTip="Format Pengisian dari Kota, Kecamatan dan kelurahan disertai dengan jalan yang dituju" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
  </div>
 <asp:Button ID="finish" runat="server" class="btn btn-info" Text="Finish" 
      onclick="finish_Click" />


    <asp:Label ID="info" runat="server" Text="Label"></asp:Label>
</asp:Content>
