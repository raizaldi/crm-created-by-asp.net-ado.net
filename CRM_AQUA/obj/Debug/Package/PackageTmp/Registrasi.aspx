﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrasi.aspx.cs" Inherits="CRM_AQUA.Registrasi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


  <div class="form-group">
    <label for="exampleInputEmail1">Nama Pelanggan</label>
     <asp:TextBox ID="nama" runat="server" class="form-control" placeholder="Nama Pelanggan" width="300px"></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Alamat Pelanggan</label>
      <asp:TextBox ID="alamat" TextMode="MultiLine" runat="server" class="form-control" placeholder="Alamat Pelanggan" width="500px"></asp:TextBox>
   </div>
  <div class="form-group">
   <label for="exampleInputPassword1">No Ktp</label>
   <asp:TextBox ID="ktp" runat="server"  class="form-control" placeholder="No Ktp" width="300px" ></asp:TextBox>
   </div>
  <div class="form-group">
    <label for="exampleInputPassword1">No NPWP</label>
   <asp:TextBox ID="npwp" runat="server"  class="form-control" placeholder="No Npwp" width="300px" ></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">No Telpon</label>
   <asp:TextBox ID="telpon" runat="server"  class="form-control" placeholder="No Telpon" width="300px" ></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Username</label>
   <asp:TextBox ID="username" runat="server"  class="form-control" placeholder="Username"  width="300px" ></asp:TextBox>
  </div>
   <div class="form-group">
    <label for="exampleInputPassword1">password</label>
   <asp:TextBox ID="password" TextMode="Password" runat="server"  class="form-control" placeholder="Password" width="300px" ></asp:TextBox>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Tanggal Hari ini</label>
   <asp:TextBox ID="tanggal" TextMode="Date" runat="server"  class="form-control" placeholder="Tanggal" width="300px" ></asp:TextBox>
  </div>
  <div class="form-group">
      <asp:DropDownList ID="nama_sales" runat="server" class="form-control" Width="300px">
        <asp:ListItem Value="">.:-Rekomendasi Sales-:.</asp:ListItem>
        <asp:ListItem Value="Dedi">Dedi</asp:ListItem>
        <asp:ListItem Value="Gama">Gama</asp:ListItem>
         <asp:ListItem Value="Asep">Asep</asp:ListItem>
        <asp:ListItem Value="Tia">Tia</asp:ListItem>
      </asp:DropDownList>
  </div>

  <asp:Button ID="btnRegistrasi" runat="server" Text="Registrasi" 
        class="btn btn-success" onclick="btnRegistrasi_Click" />

    <asp:Label ID="informasi" runat="server" Text="Label"></asp:Label>

</asp:Content>
