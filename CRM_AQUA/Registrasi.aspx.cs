﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA
{
    public partial class Registrasi : System.Web.UI.Page
    {
        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);

            if (!IsPostBack) {
                informasi.Visible = false;
            }
        }

        public void simpanRegistrasi()
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [pelanggan]
                                   ([nama_pelanggan]
                                   ,[alamat_pelanggan]
                                   ,[ktp]
                                   ,[npwp]
                                   ,[no_telpon]
                                   ,[username]
                                   ,[password]
                                   ,[tgl_berlangganan]
                                   ,[hak_akses],[nama_sales])
                             VALUES
                                   ('" + nama.Text+"','"+alamat.Text+"','"+ktp.Text+"','"+npwp.Text+"','"+telpon.Text+"','"+username.Text+"','"+password.Text+"','"+tanggal.Text+"','user','"+nama_sales.SelectedValue+"')";
                cmd.ExecuteNonQuery();
                nama.Text = "";
                alamat.Text = "";
                ktp.Text = "";
                npwp.Text = "";
                telpon.Text = "";
                username.Text = "";
                password.Text = "";
                tanggal.Text = "";
                informasi.Visible = true;
                informasi.Text = "Registrasi Berhasil dan silahkan login kembali";
            }
            catch (Exception ex) {
                informasi.Visible = true;
                informasi.Text = ex.Message;
            }
        }

        protected void btnRegistrasi_Click(object sender, EventArgs e)
        {
            simpanRegistrasi();
        }
    }
}