﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace CRM_AQUA
{
    public partial class Pemesanan : System.Web.UI.Page
    {


        private SqlConnection conn;
        public SqlCommand cmd;
        private SqlDataAdapter da;
        private DataTable dt;
        private SqlDataReader dr;
        string sqlconn;
        protected void Page_Load(object sender, EventArgs e)
        {
           // tanggal = DateTime.Now.ToLongDateString();
            //tgl.Text = tanggal;
            sqlconn = WebConfigurationManager.ConnectionStrings["koneksicrm"].ToString();
            conn = new SqlConnection(sqlconn);
            if (Session["nama_pelanggan"] == null)
            {
                Response.Redirect("Registrasi.aspx");
            }
            hitung();
            tampilpesanan();
            nilaiTertinggi();
            if (!IsPostBack) {
                info.Visible = false;
            }
            idpelanggan.Text = Session["id_pelanggan"].ToString();
        }

        public void nilaiTertinggi()
        {
            try
            {
                int hitung;
                string urut;
                conn.Open();
                string sql = @"SELECT count(no_pemesanan) as no FROM [H_pemesanan]";
                cmd = new SqlCommand(sql, conn);
                dr = cmd.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                {
                    urut = dr["no"].ToString();
                    hitung = int.Parse(urut);
                    hitung = hitung + 1;
                    nomor.Text = hitung.ToString();

                }
                else
                {
                    nomor.Text = "1";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                nomor.Text = ex.Message;
            }
        }
        public void hitung() {
            try {
                int a = int.Parse(hapro.Text);
                int b = int.Parse(jumpe.Text);
                int c = a * b;
                string hasil = c.ToString();
                juha.Text = hasil;
            }
            catch (Exception) { }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            try {
                conn.Open();
                string sql = @"SELECT [nama_produk],[harga] FROM [produk] where [id_produk] = '"+nopro.Text+"'";
                cmd = new SqlCommand(sql, conn);
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        napro.Text = dr["nama_produk"].ToString();
                        hapro.Text = dr["harga"].ToString();
                    }
                }
                
            }
            catch (Exception ex) {
                nopro.Text = ex.Message;
            }
        }
        public void jumlahBayar() {
            try {
                conn.Open();
                string sql = @"SELECT sum([jumlah_harga]) as [total] FROM [D_pemesanan] WHERE no_pemesanan='" + nomor.Text + "' ";
                cmd = new SqlCommand(sql, conn);
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        total.Text = dr["total"].ToString();
                    }
                }
            }catch (Exception) { }
        }
        public void tampilpesanan() {
            string sql = @"SELECT a.[no_detail] as [no]
                          ,a.[id_produk] as [id]
                          ,a.[quantity] as [qty]
                          ,a.[harga] as [hg]
                          ,a.[jumlah_harga] as [jml]
                          ,a.[no_pemesanan] as [pms]
                          ,b.[nama_produk] as [np]
                      FROM [D_pemesanan] a inner join [produk] b
                      on a.id_produk=b.id_produk where a.no_pemesanan='" + nomor.Text + "'";
            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            da.SelectCommand = cmd;
            SqlCommandBuilder sb = new SqlCommandBuilder(da);
            da.Fill(dt);
            detailpesanan.DataSource = dt;
            detailpesanan.DataBind();
            cmd.Dispose();
            conn.Close();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [crm_aqua].[dbo].[D_pemesanan]
                                           ([id_produk]
                                           ,[quantity]
                                           ,[harga]
                                           ,[jumlah_harga]
                                           ,no_pemesanan)
                                     VALUES
                                           ('" + nopro.Text+"','"+jumpe.Text+"','"+hapro.Text+"','"+juha.Text+"','"+nomor.Text+"')";
                cmd.ExecuteNonQuery();
                tampilpesanan();
                jumlahBayar();
                nopro.Text = "";
                jumpe.Text = "";
                hapro.Text = "";
                juha.Text = "";
                napro.Text = "";
                conn.Close();
            }
            catch (Exception) { }
        }
        public void simpanPemesanan() {
            try {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO H_pemesanan(no_pemesanan,tgl_pemesanan,id_pelanggan,total_pemesanan) OUTPUT INSERTED.no_pemesanan,'" + tanggalkirim.Text + "','" + alamat.Text + "' INTO pengiriman (no_pemesanan,tgl_pengiriman,alamat_pengiriman) VALUES  ('" + nomor.Text + "','" + tgl.Text + "','" + idpelanggan.Text + "','" + total.Text + "');";
                cmd.ExecuteNonQuery();
                conn.Close();
                nomor.Text = "";
                total.Text = "";
                tgl.Text = "";
                tanggalkirim.Text = "";
                alamat.Text = "";
                idpelanggan.Text = "";
                info.Visible = true;
                info.Text = "Sukses";
            }
            catch (Exception ex) {
                idpelanggan.Text = ex.Message;
            }
        }
        protected void finish_Click(object sender, EventArgs e)
        {
            simpanPemesanan();
            nilaiTertinggi();
            tampilpesanan();
        }
    }
}