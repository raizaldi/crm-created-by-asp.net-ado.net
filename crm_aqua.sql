CREATE DATABASE [crm_aqua]
GO
USE [crm_aqua]
GO
/****** Object:  Table [dbo].[produk]    Script Date: 06/12/2016 01:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[produk](
	[id_produk] [varchar](5) NOT NULL,
	[nama_produk] [varchar](50) NULL,
	[jenis_produk] [varchar](50) NULL,
	[harga] [decimal](7, 0) NULL,
	[foto] [varchar](50) NULL,
	[member] [varchar](5) NULL,
 CONSTRAINT [PK_produk] PRIMARY KEY CLUSTERED 
(
	[id_produk] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pengiriman]    Script Date: 06/12/2016 01:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pengiriman](
	[no_pengiriman] [int] IDENTITY(1,1) NOT NULL,
	[tgl_pengiriman] [date] NULL,
	[no_pemesanan] [int] NULL,
	[alamat_pengiriman] [varchar](200) NULL,
 CONSTRAINT [PK_pengiriman] PRIMARY KEY CLUSTERED 
(
	[no_pengiriman] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pelanggan]    Script Date: 06/12/2016 01:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pelanggan](
	[id_pelanggan] [int] IDENTITY(1,1) NOT NULL,
	[nama_pelanggan] [varchar](50) NULL,
	[alamat_pelanggan] [varchar](100) NULL,
	[ktp] [varchar](25) NULL,
	[npwp] [varchar](15) NULL,
	[no_telpon] [varchar](15) NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[tgl_berlangganan] [date] NULL,
	[hak_akses] [varchar](50) NULL,
 CONSTRAINT [PK_pelanggan] PRIMARY KEY CLUSTERED 
(
	[id_pelanggan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[komplain]    Script Date: 06/12/2016 01:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[komplain](
	[no_komplain] [int] NOT NULL,
	[keterangan] [text] NULL,
	[id_pelanggan] [int] NULL,
	[konfirmasi] [text] NULL,
 CONSTRAINT [PK_komplain] PRIMARY KEY CLUSTERED 
(
	[no_komplain] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[H_pemesanan]    Script Date: 06/12/2016 01:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[H_pemesanan](
	[no_pemesanan] [int] NOT NULL,
	[tgl_pemesanan] [date] NULL,
	[id_pelanggan] [int] NULL,
	[total_pemesanan] [decimal](9, 0) NULL,
 CONSTRAINT [PK_H_pemesanan] PRIMARY KEY CLUSTERED 
(
	[no_pemesanan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[D_pemesanan]    Script Date: 06/12/2016 01:33:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[D_pemesanan](
	[no_detail] [int] IDENTITY(1,1) NOT NULL,
	[id_produk] [varchar](5) NULL,
	[quantity] [int] NULL,
	[harga] [decimal](7, 0) NULL,
	[jumlah_harga] [decimal](7, 0) NULL,
	[no_pemesanan] [int] NULL,
 CONSTRAINT [PK_D_pemesanan] PRIMARY KEY CLUSTERED 
(
	[no_detail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
